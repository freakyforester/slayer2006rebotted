package Slayer;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.GroundItem;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;
import org.rev317.min.api.wrappers.TilePath;

import TaverlyChaos.ChaosDruids;

//It is better practice to separate strategies into another class, but for beginner's sake we'll keep it all in one
//This strategy is in charge of catching fish
public class GetTask implements Strategy {
	static final Tile tileTurael = new Tile(2928, 3536, 0);
	private static final Tile[] walkToTurael = {
			new Tile(3226, 3218, 0),
	        new Tile(3226, 3218, 0),
	        new Tile(3232, 3219, 0),
	        new Tile(3231, 3225, 0),
	        new Tile(3231, 3231, 0),
	        new Tile(3225, 3236, 0),
	        new Tile(3220, 3242, 0),
	        new Tile(3220, 3248, 0),
	        new Tile(3218, 3254, 0),
	        new Tile(3216, 3260, 0),
	        new Tile(3215, 3262, 0),
	        new Tile(3215, 3262, 0),
	        new Tile(3216, 3268, 0),
	        new Tile(3215, 3274, 0),
	        new Tile(3211, 3278, 0),
	        new Tile(3211, 3278, 0),
	        new Tile(3205, 3280, 0),
	        new Tile(3199, 3280, 0),
	        new Tile(3193, 3281, 0),
	        new Tile(3187, 3281, 0),
	        new Tile(3181, 3284, 0),
	        new Tile(3178, 3286, 0),
	        new Tile(3178, 3286, 0),
	        new Tile(3172, 3288, 0),
	        new Tile(3167, 3291, 0),
	        new Tile(3165, 3297, 0),
	        new Tile(3163, 3303, 0),
	        new Tile(3158, 3309, 0),
	        new Tile(3153, 3315, 0),
	        new Tile(3151, 3321, 0),
	        new Tile(3150, 3327, 0),
	        new Tile(3148, 3333, 0),
	        new Tile(3147, 3339, 0),
	        new Tile(3143, 3345, 0),
	        new Tile(3140, 3351, 0),
	        new Tile(3134, 3356, 0),
	        new Tile(3131, 3361, 0),
	        new Tile(3131, 3367, 0),
	        new Tile(3128, 3373, 0),
	        new Tile(3125, 3378, 0),
	        new Tile(3122, 3382, 0),
	        new Tile(3117, 3386, 0),
	        new Tile(3114, 3389, 0),
	        new Tile(3108, 3392, 0),
	        new Tile(3102, 3392, 0),
	        new Tile(3096, 3392, 0),
	        new Tile(3090, 3393, 0),
	        new Tile(3084, 3395, 0),
	        new Tile(3078, 3397, 0),
	        new Tile(3072, 3401, 0),
	        new Tile(3066, 3402, 0),
	        new Tile(3060, 3402, 0),
	        new Tile(3054, 3402, 0),
	        new Tile(3048, 3403, 0),
	        new Tile(3042, 3405, 0),
	        new Tile(3036, 3408, 0),
	        new Tile(3030, 3408, 0),
	        new Tile(3024, 3408, 0),
	        new Tile(3018, 3410, 0),
	        new Tile(3012, 3412, 0),
	        new Tile(3006, 3415, 0),
	        new Tile(3000, 3419, 0),
	        new Tile(2994, 3420, 0),
	        new Tile(2988, 3421, 0),
	        new Tile(2982, 3423, 0),
	        new Tile(2977, 3427, 0),
	        new Tile(2971, 3432, 0),
	        new Tile(2965, 3435, 0),
	        new Tile(2960, 3437, 0),
	        new Tile(2954, 3440, 0),
	        new Tile(2948, 3442, 0),
	        new Tile(2944, 3448, 0),
	        new Tile(2938, 3450, 0),
	        new Tile(2936, 3451, 0),
	        new Tile(2934, 3450, 0),
	        new Tile(2934, 3450, 0),
	        new Tile(2935, 3456, 0),
	        new Tile(2938, 3461, 0),
	        new Tile(2938, 3467, 0),
	        new Tile(2935, 3471, 0),
	        new Tile(2935, 3477, 0),
	        new Tile(2935, 3483, 0),
	        new Tile(2935, 3489, 0),
	        new Tile(2934, 3495, 0),
	        new Tile(2934, 3501, 0),
	        new Tile(2930, 3507, 0),
	        new Tile(2925, 3511, 0),
	        new Tile(2923, 3517, 0),
	        new Tile(2923, 3523, 0),
	        new Tile(2923, 3529, 0),
	        new Tile(2928, 3535, 0),
	        new Tile(2928, 3536, 0)
	};
	static TilePath walkMaster1 = new TilePath(walkToTurael);
 @Override
 public boolean activate() {
         return Slayer.taskid == 0 && Slayer.previoustaskid == 0;
 }

 @Override
 public void execute() {
	 if (Slayer.slayerMaster == 1) {
     	while(Players.getMyPlayer().isInCombat()) {
     		Slayer.status = "Waiting to be out of combat...";
    		Time.sleep(250,400);
    		/*
    		 * Sleeping while in combat otherwise using ::stuck 1 teleport is an ass....
    		 */
    	}
         Slayer.status = "Walking to Turael";
         Slayer.task = "None";
         if (tileTurael.distanceTo() > 20) {
         Keyboard.getInstance().sendKeys("::stuck 1");
         Time.sleep(3000);
         while(!walkMaster1.hasReached()) {
         	walkMaster1.traverse();
         	Time.sleep(2000, 2500);
         }
         }
     	Npc[] npcs = Npcs.getNearest(70);
     	if (npcs != null && npcs.length > 0) {
     		for (Npc n : npcs){
     			if (n != null && !n.isInCombat() ) {
     				//nothing null
     				
     				n.interact(Npcs.Option.TALK_TO);
     				
     				Time.sleep(10000);
     				Menu.sendAction(679, 1246, 275, 4886);
     				Time.sleep(4000);
     				Menu.sendAction(315, 1246, 275, 2482);
     				Time.sleep(4000);
     				//Send Keys to close dialogue
     				Menu.sendAction(679, 1246, 267, 4892);
     				//
     				Time.sleep(4000);
     			}
     		}
     	}
	 }
 
 }
 

}