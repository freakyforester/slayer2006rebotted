package Slayer;

import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.api.utils.Timer;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.Loader;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.wrappers.Item;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;

import Slayer.Strategies.KillBanshees;
import Slayer.Strategies.KillBears;
import Slayer.Strategies.KillGreenGoblins;
import Slayer.Strategies.KillRedGoblins;
import Slayer.Strategies.KillScorpions;
import Slayer.Strategies.KillSkeletons;
import Slayer.Strategies.Wait;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
@ScriptManifest(author = "JetFox",
category = Category.SLAYER,
description = "Slayer",
name = "Slayer",
servers = { "All" },
version = 0.3)
public class Slayer extends Script implements Paintable, MessageListener {
	//TASKID LIST
	//TASKID 1 = Bears
	//TASKID 2 = Scorpion HAS STUCK USAGE
	//TASKID 3 = Skeletons
	//TASKID 4 = Red Goblins
	//TASKID 5 = Green Goblins
	//TASKID 6 = Banshees HAS STUCK USAGE
	//END TASKID LIST
    private Timer timer;
    private int killsLeft;
    public static String task;
    private int taskCompleted;
    public static int taskid = 0;
    public static int previoustaskid = 0;
    public static int slayerMaster;
    public static Npc NpcSpot;
    // This is a simple String that shows what part of the script we're on
    public static String status;
    public static String accPass;
    public static String accUser;
    public static int food;
    public static int food2;
    public static int foodAmt;
    public static int healthEat;
    // This ArrayList will hold all our strategies (Drop, Wait and CatchFish)
    private final ArrayList<Strategy> strategies = new ArrayList<>();
    @Override
    public boolean onExecute()
    {
        // Let's initialize some data
        // This makes it so the timer starts running
        timer = new Timer();

        // This changes what the status of our script is at.
        status = "Initializing the script";
        try
        {
            // Pops up a small window to allow the user to input a custom fishing spot id
            accUser = JOptionPane.showInputDialog(null, "Please enter the account username");

            // Same as above, only it is for the fishing tool id
            accPass = JOptionPane.showInputDialog(null, "Please enter the account password");
            
            slayerMaster = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter slayer master id (1 = turael, 2 = Mazchna, 3 = Vannaka, 4 = Chaeldar... etc)"));
            //food = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter food ID (Not the +1 number)"));
            //foodAmt = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter amount of food to withdraw"));
            //food2 = food + 1;
            //healthEat = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter health to eat at"));
            taskid = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter current taskid: 0=No Task, 1 = Bears, 2 = Scorpion, 3 = Skeletons, 4 = Red Goblins, 5 = Green Goblins, 6 = Banshees"));
        }
        catch (NumberFormatException e)
        {
            setState(STATE_STOPPED);

            JOptionPane.showMessageDialog(null, "There was an error when getting the ids! Please try again.");
        }
        strategies.add(new GetTask());
        strategies.add(new GetEquipment());
        strategies.add(new KillBears());
        strategies.add(new KillBanshees());
        strategies.add(new KillScorpions());
        strategies.add(new KillSkeletons());
        strategies.add(new KillRedGoblins());
        strategies.add(new KillGreenGoblins());
        strategies.add(new Wait());
        strategies.add(new Walker());
        provide(strategies);
        return true;
    }

    @Override
    public void paint(Graphics g)
    {
        // Sets the color for the text
        g.setColor(Color.WHITE);
        // Sets the font to a 14-point Helvetica
        g.setFont(new Font("Helvetica", Font.PLAIN, 14));

        // g.drawString(String message, int x, int y);
        // The above is the format for how a drawString generally works
        // X and y are the coordinates on Parabot where the String will appear
        // (0, 0) is in the top-left
        g.drawString("Timer: " + timer.toString(), 10, 15);
        g.drawString("Status : " + status, 10, 30);
        g.drawString("Task : " + task, 10, 45);
        g.drawString("Tasks Completed : " +taskCompleted, 10, 60);
    }
    public void messageReceived(MessageEvent message) {
        switch (message.getType()) {
            case 0: // system message
                if (message.getMessage().startsWith("Congratulations, you just advanced a")) {
                    // add in level up to paint
                }
                if (message.getMessage().startsWith("You completed your slayer task")) {
                	if (taskid == 1) {
                		//bear path reverse
                		previoustaskid = 1;
                	}
                	else if (taskid == 4 || taskid == 5) {
                		//goblins reverse path
                		previoustaskid = 4;
                	}
                	
                    taskid = 0;
                    task = "None";
                    taskCompleted = taskCompleted + 1;
                    
                }
                if (message.getMessage().startsWith("You have been assigned")) {
                	if (message.getMessage().contains("bear,")) {
                		taskid = 1;
                		task = "Bears";
                		status = "Walking to bears";
                	}
                	if (message.getMessage().contains("scorpion,")) {
                		taskid = 2;
                		task = "Scorpions";
                		status = "Walking to scorpions";
                	}
                	if (message.getMessage().contains("skeleton,")) {
                		taskid = 3;
                		task = "Skeletons";
                		status = "Walking to skeletons";
                	}
                	if (message.getMessage().contains("red goblin,")) {
                		taskid = 4;
                		task = "Red Goblins";
                		status = "Walking to Goblins";
                	}
                	if (message.getMessage().contains("green goblin,")) {
                		taskid = 5;
                		task = "Green Goblins";
                		status = "Walking to Goblins";
                	}
                	if (message.getMessage().contains("banshee,")) {
                		taskid = 6;
                		task = "Banshees";
                		status = "Walking to Banshees";
                	}
                }
                break;
            case 4: // trade request
                break;
        }
    }

}
