package Slayer.Strategies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.Tile;
import org.rev317.min.api.wrappers.TilePath;

import Slayer.Slayer;


public class KillGreenGoblins implements Strategy
{	
	public static final Tile tileGoblins = new Tile(2956, 3504);
    @Override
    public boolean activate()
    {
    	if(!Players.getMyPlayer().isInCombat() && Slayer.taskid == 5 && tileGoblins.distanceTo() < 20) {
    		Slayer.status = "Finding Red Goblins!";
    		Slayer.task = "Red Goblins";
            // Makes sure there are NPC spots with the predetermined id nearby //
    	try {
    	Npc[] npcs = Npcs.getNearest(298);
    	if (npcs != null && npcs.length > 0) {
    		for (Npc n : npcs){
    			if (n != null && !n.isInCombat() ) {
    				//nothing null
    				Slayer.NpcSpot = n;
    				return true;
    			}
    		}
    	}
    	} catch(Exception e) {
    		return false;
    	}
    	}

        return false;
    }

    @Override
    public void execute()
    {

    		if (Slayer.NpcSpot != null)
            {
                // interact(0) is basically left-clicking the fishingSpot
            	if (!Slayer.NpcSpot.isInCombat() && !Players.getMyPlayer().isInCombat()){
            		Slayer.status = "I'm Gonna fuck Up that Green Goblin!";
            		Slayer.NpcSpot.interact(Npcs.Option.ATTACK);
            		Time.sleep(1000);
            	}
                
                // Sleep conditions are arguably the most important part of this script
                // A sleep condition is basically a sleep that stops when our condition is met
                Time.sleep(new SleepCondition()
                {
                    @Override
                    public boolean isValid()
                    {
                        // Our condition is met when our animation is not the default (-1)
                        // If it isn't the default, we can assume we are now fishing and can finish the sleep condition
                    	return Slayer.NpcSpot.isInCombat() && Players.getMyPlayer().isInCombat()
								&& Slayer.NpcSpot.distanceTo() >= 10;
                    }
                // The max this script will sleep for is 5 seconds
                }, 5000);
            }

        // Null checks are always good to use for a more stable script
    }
}