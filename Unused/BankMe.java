package Slayer;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.Tile;

public class BankMe implements Strategy
{
    @Override
    public boolean activate()
    {
        // This strategy activates when our inventory is full
        return Inventory.isFull() || !Inventory.contains(Slayer.food2);
    }

    @Override
    public void execute()
    {
        
        if(!Players.getMyPlayer().getLocation().equals(new Tile(3185, 3436))){
        	Slayer.status = "Walking to the bank!";
        	//Interact with ladder to escape.

            while(!Slayer.walkBank.hasReached()) {
            	Slayer.walkBank.traverse();
            	Time.sleep(1000, 2000);
            }
            if (Slayer.walkBank.hasReached()) {

            	Slayer.status = "Banking!";
            	Npc banker = Npcs.getClosest(2619);
                
                while (!Bank.isOpen()) {
                	banker.interact(Npcs.Option.BANK);
                    Time.sleep(1000);
                }
                if (Bank.isOpen()) {
                    Bank.depositAllExcept();
                    Bank.withdraw(Slayer.food2, Slayer.foodAmt, 2000);
                    Time.sleep(1000);
                
                }
                Slayer.status = "Walking Back!";
                /*
                 * Walker should automatically take over if Kill*****.java is out of range.
                */
            }
        }
    }
}
