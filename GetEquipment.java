package Slayer;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Equipment;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.GroundItem;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;
import org.rev317.min.api.wrappers.TilePath;

import TaverlyChaos.ChaosDruids;

//It is better practice to separate strategies into another class, but for beginner's sake we'll keep it all in one
//This strategy is in charge of catching fish
public class GetEquipment implements Strategy {
 @Override
 public boolean activate() {
	 if (Slayer.taskid == 6 && !Equipment.isWearing(4167)) {
		 return true;
	 }
        return false;
 }

 @Override
 public void execute() {
	 if (Slayer.taskid == 6) {
	 if (!Equipment.isWearing(4167)) {
         Slayer.status = "Getting Earmuffs";
         Slayer.task = "Banshees";
         if (Inventory.contains(4167)) {
        	 Slayer.status = "Equipping Earmuffs";
        	 Menu.sendAction(454, 4166, Inventory.getItems(4167)[0].getSlot(), 3214);
        	 Time.sleep(1000);
         }
         else
         {
        	 //implement bankme.java, probably just set a variable that will use bank instead of implementing bank for every single taskid
        	 Slayer.status = "Error: Earmuff BankMe not yet implemented.";
        	 
         }
	 }
	 }
 }
 }